#include <stdio.h>
#include <getopt.h>
#include <unistd.h>
#include <stdbool.h>

static void help_message(char* program_name)
{
	fprintf(stderr,
		"Usage: %s <option>\n\
options: \n\
	-i pkg_name [installs the package] \n\
	-u pkg_name [uninstalls the package] \n\
	-r pkg_name [removes the package] \n\
	-v	    [enable verbose output]\n",
		program_name);
	return;
}

int main(int argc, char **argv)
{
	bool install_program;
	bool uninstall_program;
	bool remove_program;
	bool verbose;
	int opt;

	while ((opt = getopt(argc, argv, "i:u:r:v:")) != 1) {
		switch (opt) {
			case 'i':
				install_program = true;
				break;
			case 'u':
				uninstall_program = true;
				printf("uninstall a program");
				break;
			case 'r':
				remove_program = true;
				break;
			case 'v':
				verbose = true;
				break;
			default:
				help_message(argv[0]);
				return 1;
		}
	}
	
	return 0;
}
